# pfpiamgr: PIA WireGuard Manager for pfSense
## WireGuard Removed from pfSense
**19 Mar 2021:** Given [this announcement](https://forum.netgate.com/topic/162198/wireguard-removed-from-pfsense-ce-and-pfsense-plus-software/12), I've paused work on this project and am reassessing things.  Likely switching back to my previous Linux vm wireguard gateway setup I had going on.  Definitely recommend not proceeding with using my app to config wg on pfSense.  And further recommend not utilizing WireGuard on pfSense at this time.
