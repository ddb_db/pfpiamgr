#### pfpiamgr configuration sample ####
## A sample configuration file to get you started.

### PIA creds

# Your PIA login id (required)
pfpiamgr.pia.id=[id]

# Your PIA login password (required)
pfpiamgr.pia.password=[password]

### App settings

# Your pfSense hostname or IP (required)
pfpiamgr.pfsense-host=192.168.0.1

# Your pfSense login id; root assumed if not specified
pfpiamgr.pfsense-login=root

# Only login via public key auth supported; where are the ssh keys to login?
# If not specified, default location used (i.e. ~/.ssh/); specify the dir
# AND the private key file when setting this file (i.e. /sshkeys/id_rsa)
pfpiamgr.ssh-key=

# How frequently should port forwards be refreshed with PIA?
# MUST be in java.time.Duration format (i.e. PT3M for a three minute interval)
# default is 3 minutes when not specified
pfpiamgr.port-forward-refresh=PT3M

# How often should the active interfaces be tested?
# MUST be in java.time.Duration format (i.e. PT2M for a two minute interval)
# default is 2 minutes when not specified
pfpiamgr.interface-test-interval=PT2M

# How often should the regions be pinged?
# MUST be in java.time.Duration format (i.e. PT30M for a 30 minute interval)
# default is 30 minutes when not specified
pfpiamgr.region-ping-interval=PT30M

# Max milliseconds to wait for responses from PIA region servers (50-2000)
pfpiamgr.max-region-response-millis=250

# Number of threads used to ping regions (2-16)
pfpiamgr.region-ping-threads=8

# How often should the region cache be expired? (60-3600)
pfpiamgr.region-cache-age-seconds=1800

# What is the url for the PIA regions data?
pfpiamgr.pia-regions-url=https://serverlist.piaservers.net/vpninfo/servers/v4

# How many samples are taken from each region when pinging? (3-10)
pfpiamgr.region-ping-samples=3

# How many tries before declaring internet down?
pfpiamgr.internet-alive-retries=3

# How many tries before declaring a PIA interface down?
pfpiamgr.interface-alive-retries=3

### One interface config; repeat with an incremented index for each additional
### interface you want managed by piawgmgr

# The name of this interface to be created (required)
pfpiamgr.interfaces[0].name=

# The PIA region to connect this interface to (required)
pfpiamgr.interfaces[0].region=

# Should port forwarding be enabled on this interface? Default is false. [true|false]
pfpiamgr.interfaces[0].port-forward=

# The defined persistent keep alive value for the WireGuard interface (in seconds)
pfpiamgr.interfaces[0].persistent-keep-alive=

# If you wanted to add an additional interface, just increment the index; the
# name must be unique across all defined interfaces.
pfpiamgr.interfaces[1].name=
pfpiamgr.interfaces[1].region=
pfpiamgr.interfaces[1].port-forward=
pfpiamgr.interfaces[1].persistent-keep-alive=
