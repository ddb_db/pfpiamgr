FROM openjdk:8-jre-slim

COPY pfpiamgr-boot/ /opt/pfpiamgr/
RUN ["useradd", "-ms", "/bin/bash", "pfpiamgr"]

VOLUME /sshkeys
VOLUME /etc/pfpiamgr

USER pfpiamgr
ENTRYPOINT ["/opt/pfpiamgr/bin/pfpiamgr", "--spring.config.location=/etc/pfpiamgr/pfpiamgr.properties"]
