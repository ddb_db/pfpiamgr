/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.WgServerBuilder
import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import spock.lang.Specification
import spock.lang.Unroll

class WgCliServiceImplTest extends Specification {

    private SystemService systemService

    def setup() {
        systemService = GroovyStub(SystemService)
    }

    @Unroll
    def 'when the first #pingFailures ping fail, isInterfaceAlive() returns #expectedResult'() {
        setup:
            def wgSettings = GroovyStub(WgSettingsConfig) {
                getInterfaceAliveRetries() >> 3
            }
            def sshService = GroovyMock(SshService) {
                Math.min(pingFailures + 1, 3) * exec({ it.startsWith('ping') }) >>> generateFailedCommandCalls(pingFailures)
                0 * _
            }
        when:
            def result = new WgCliServiceImpl(Stub(HttpClientService), Stub(WgServerBuilder), Stub(VpnRegionService),
                            Stub(WgInterfaceConfig), wgSettings, sshService, systemService).isInterfaceAlive(GroovyStub(WgInterface))
        then:
            result == expectedResult
        where:
            pingFailures    || expectedResult
            0               || true
            1               || true
            2               || true
            3               || false
    }

    @Unroll
    def 'when the first #pingFailures pings fail, isInternetAlive() returns #expectedResult'() {
        setup:
            def wgSettings = GroovyStub(WgSettingsConfig) {
                getInternetAliveRetries() >> 3
            }
            def sshService = GroovyMock(SshService) {
                Math.min(pingFailures + 1, 3) * exec({ it.startsWith('ping') }) >>> generateFailedCommandCalls(pingFailures)
                0 * _
            }
        when:
            def result = new WgCliServiceImpl(Stub(HttpClientService), Stub(WgServerBuilder), Stub(VpnRegionService),
                Stub(WgInterfaceConfig), wgSettings, sshService, systemService).isInternetAlive()
        then:
            result == expectedResult
        where:
            pingFailures    || expectedResult
            0               || true
            1               || true
            2               || true
            3               || false
    }



    def generateFailedCommandCalls(int failedCalls) {
        def results = []
        for(int i = 0; i < failedCalls; ++i)
            results << new SshService.ExecResult(status: 1)
        results << new SshService.ExecResult(status: 0)
        results
    }
}