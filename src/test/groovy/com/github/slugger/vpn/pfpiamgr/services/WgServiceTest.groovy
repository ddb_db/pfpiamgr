package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.WgServerBuilder
import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import com.github.slugger.vpn.pfpiamgr.entities.WgServer
import spock.lang.Specification

class WgServiceTest extends Specification {

    AppInitService appInitService

    def setup() {
        appInitService = Stub() {
            isInitDone() >> true
        }
    }

    def 'when an interface is down, it gets recreated'() {
        setup:
            def wgSettingsConfig = GroovyMock(WgSettingsConfig) {
                0 * _
            }
            def svrBuilder = GroovyMock(WgServerBuilder) {
                0 * _
            }
            def regions = GroovyMock(VpnRegionService) {
                0 * _
            }
            def http = GroovyMock(HttpClientService) {
                0 * _
            }
            def cli = GroovyMock(WgCliService) {
                1 * isInterfaceAlive(_) >> false
                1 * isInternetAlive() >> true
                1 * configInterfaces()
                1 * alert(_)
                0 * _
            }
            def activeInterface = GroovyMock(WgInterface) {
                1 * getWgServer() >>> [GroovyStub(WgServer), null]
                1 * setWgServer(null)
                _ * getPersistentKeepAlive() >> 0
                _ * /get.+/() >> 'active'
                _ * hashCode() >> 'active'.hashCode()
                0 * _
            }
            def wgInterfaceConfig = GroovyMock(WgInterfaceConfig) {
                1 * getInterfaces() >> ([activeInterface] as HashSet<WgInterface>)
                0 * _
            }
        when:
           new WgService(http, regions, cli, wgInterfaceConfig, wgSettingsConfig, svrBuilder, Stub(PortForwardService), appInitService).testActiveInterfaces()
        then:
            notThrown(Throwable)
    }

    def 'testInterfaces() only tests those with a valid WgServer attached'() {
        setup:
            def cli = GroovyMock(WgCliService) {
                1 * isInterfaceAlive(_) >> true
                0 * _
            }
            def activeInterface = GroovyMock(WgInterface) {
                1 * getWgServer() >> GroovyStub(WgServer)
                2 * getName() >> 'active'
                0 * _
            }
            def downInterface = GroovyMock(WgInterface) {
                1 * getWgServer()
                0 * _
            }
            def wgInterfaceConfig = GroovyMock(WgInterfaceConfig) {
                1 * getInterfaces() >> ([downInterface, activeInterface] as HashSet<WgInterface>)
                0 * _
            }
        when:
            new WgService(Stub(HttpClientService), Stub(VpnRegionService), cli, wgInterfaceConfig, Stub(WgSettingsConfig), new WgServerBuilder(), Stub(PortForwardService), appInitService).testActiveInterfaces()
        then:
            notThrown(Throwable)
    }
}