package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.PiaConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.factories.RestClientThreadLocalFactory
import groovy.json.JsonOutput
import groovyx.net.http.RESTClient
import spock.lang.Specification
import spock.lang.Unroll

class HttpClientServiceTest extends Specification {
    def 'when registerWgPublicKey is called, the expected http request is made to PIA'() {
        setup:
            def publicKey = 'pubkey'
            def piaToken = 'piatoken'
            def region = [servers: [wg:[[ip: '1.1.1.1', cn: 'cn']], meta: [[ip: '2.2.2.2']]]]
            def restClient = Mock(RESTClient) {
                2 * setClient(_) // once for each endpoint requested
                1 * get({
                    it.uri == "https://${region.servers.wg[0].ip}:1337/addKey"
                }) >> [data: [status: 'OK']]
                1 * get({
                    it.uri == "https://${region.servers.meta[0].ip}/authv3/generateToken"
                }) >> [data: new StringReader(JsonOutput.toJson([status: 'OK', token: piaToken]))]
                0 * _
            }
            def restFactory = mockFactory(restClient)
            def http = new HttpClientServiceImpl(Stub(PiaConfig), new WgSettingsConfig(), restFactory)
        expect:
            http.registerWgPublicKey(publicKey, region) instanceof Map
    }

    def 'when the region cache has not expired, the endpoint is not queried'() {
        setup:
            def restClient = Mock(RESTClient) {
                0 * _
            }
            def restFactory = mockFactory(restClient)
            def http = new HttpClientServiceImpl(Stub(PiaConfig), new WgSettingsConfig(), restFactory)
            http.regionsCache.expires = new Date(System.currentTimeMillis() + 86400000L)
            http.regionsCache.data = [regions: 'foo']
        when:
            def result = http.fetchRegions()
        then:
            result == 'foo'
    }

    @Unroll
    def 'when the region cache #desc, the endpoint is queried for a data refresh'() {
        setup:
            def restClient = Mock(RESTClient) {
                1 * get(_) >> [data: [regions: 'foo']]
                0 * _
            }
            def restFactory = mockFactory(restClient)
            def http = new HttpClientServiceImpl(Stub(PiaConfig), new WgSettingsConfig(), restFactory)
            if(expiry)
                http.regionsCache.expires = expiry
        when:
            def result = http.fetchRegions()
        then:
            result == 'foo'
        where:
            expiry                                           | desc
            null                                             | 'is null'
            new Date(System.currentTimeMillis() - 86400000L) | 'has expired'
    }

    def mockFactory(def client) {
        Mock(RestClientThreadLocalFactory) {
            2 * create() >> new ThreadLocal<RESTClient>() { // one call for each of pia client and standard client
                @Override
                protected RESTClient initialValue() {
                    client
                }
            }
            0 * _
        }
    }
}