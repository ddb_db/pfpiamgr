package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import spock.lang.Specification
import spock.lang.Unroll

class VpnRegionServiceTest extends Specification {

    AppInitService appInitService

    def setup() {
        appInitService = Stub() {
            isInitDone() >> true
        }
    }

    @Unroll
    def 'when the http service returns no match [#httpResult], getRegion() returns null'() {
        setup:
            def http = Mock(HttpClientService) {
                1 * fetchRegions() >> [[id: httpResult]]
                0 * _
            }
            def vpnRegionService = new VpnRegionService(http, new WgSettingsConfig(), appInitService)
        when:
            def result = vpnRegionService.getRegion('foo')
        then:
            result == null
        where:
            httpResult << [null, 'fo', 'fooo', 'bar', 'zoo']
    }

    def 'when the http service returns a match, getRegion() returns the matching region'() {
        setup:
            def http = Mock(HttpClientService) {
                1 * fetchRegions() >> [[id: 'fooo'], [id: 'foo'], [id: 'ofoo']]
                0 * _
            }
            def vpnRegionService = new VpnRegionService(http, new WgSettingsConfig(), appInitService)
        when:
            def result = vpnRegionService.getRegion('foo')
        then:
            result.id == 'foo'
    }

    def 'when fastestRegion is not null, getFastestRegion() returns a clone'() {
        setup:
            def vpnRegionService = new VpnRegionService(Stub(HttpClientService), new WgSettingsConfig(), appInitService)
            vpnRegionService.fastestRegion = [id: 'foo']
        when:
            def result = vpnRegionService.fastestRegion
        then:
            result.id == vpnRegionService.fastestRegion.id
            !result.is(vpnRegionService.fastestRegion)
    }
}