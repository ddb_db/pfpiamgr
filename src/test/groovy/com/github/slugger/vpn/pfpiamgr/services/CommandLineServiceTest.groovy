package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.ExecResultBuilder
import spock.lang.Specification

class CommandLineServiceTest extends Specification {
    def 'when a command line is executed, the output, exit status and command line issued are returned as expected'() {
        setup:
            def rawCommandLine = ['foo', 'bar']
            def proc = GroovyStub(Process) {
                consumeProcessOutput(_, _) >> { a, b -> a << 'a'; b << 'b' }
                exitValue() >> 0
            }
            def cmdLine = GroovySpy(ArrayList) {
                1 * execute(*_) >> proc
            }
            cmdLine.addAll(rawCommandLine)
            def cli = new CommandLineService(new ExecResultBuilder())
        when:
            def result = cli.exec(cmdLine)
        then:
            result.status == 0
            result.stdout == 'a'
            result.stderr == 'b'
            result.command == rawCommandLine
    }
}