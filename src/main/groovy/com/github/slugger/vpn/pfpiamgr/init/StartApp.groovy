/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.init

import com.github.slugger.vpn.pfpiamgr.services.AppInitService
import com.github.slugger.vpn.pfpiamgr.services.VpnRegionService
import com.github.slugger.vpn.pfpiamgr.services.WgService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(99)
@Slf4j
class StartApp implements ApplicationRunner {

    private final AppInitService appInitService
    private final VpnRegionService vpnRegionService
    private final WgService wgService

    @Autowired
    StartApp(AppInitService appInitService, VpnRegionService vpnRegionService, WgService wgService) {
        this.appInitService = appInitService
        this.vpnRegionService = vpnRegionService
        this.wgService = wgService
    }

    @Override
    void run(ApplicationArguments args) throws Exception {
        if(!appInitService.initDone) {
            vpnRegionService.pingRegions(true)
            appInitService.initDone = true
        }
        wgService.init()
    }
}
