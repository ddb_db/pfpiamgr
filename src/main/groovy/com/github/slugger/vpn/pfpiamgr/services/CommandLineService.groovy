/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.ExecResultBuilder
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
// TODO We can probably just remove this class now
class CommandLineService {

    static class ExecResult {
        int status
        String stdout
        String stderr
        List<String> command
    }

    private final ExecResultBuilder execResultBuilder

    @Autowired
    CommandLineService(ExecResultBuilder execResultBuilder) {
        this.execResultBuilder = execResultBuilder
    }

    ExecResult exec(List command, boolean needsPrivilegedAccess = false, long timeoutMillis = 2000L, boolean logFailureAsWarning = false) {
        if(needsPrivilegedAccess && System.properties['user.name'] != 'root')
            command.add(0, 'sudo') // TODO don't assume sudo
        log.debug "Executing: $command"
        def p = command.execute()
        def stdout = new StringBuilder()
        def stderr = new StringBuilder()
        p.consumeProcessOutput(stdout, stderr)
        p.waitForOrKill(timeoutMillis)
        log.debug "RC: ${p.exitValue()}"
        if(p.exitValue() || log.isTraceEnabled()) {
            def logLevel = !p.exitValue() ? 'trace' : logFailureAsWarning ? 'warn' : 'error'
            if(!log.isDebugEnabled())
                log."$logLevel"("Executing: $command")
            if(stdout.toString())
                log."$logLevel"("stdout:\n$stdout")
            if(stderr.toString())
                log."$logLevel"("stderr:\n$stderr")
        }
        execResultBuilder
            .withStatus(p.exitValue())
            .withStdout(stdout.toString().trim())
            .withStderr(stderr.toString().trim())
            .withCommand(command)
            .build()
    }
}
