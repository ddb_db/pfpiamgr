/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.util.StopWatch

import javax.annotation.PreDestroy
import java.time.Duration
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

@Service
@Slf4j
class VpnRegionService {

    private final HttpClientService httpClientService
    private final AppInitService appInitService
    private final WgSettingsConfig wgSettingsConfig
    private final ExecutorService executorService
    private final List<Future<?>> activeTasks

    private def fastestRegion

    @Autowired
    VpnRegionService(HttpClientService httpClientService, WgSettingsConfig wgSettingsConfig, AppInitService appInitService) {
        this.httpClientService = httpClientService
        this.appInitService = appInitService
        this.wgSettingsConfig = wgSettingsConfig
        fastestRegion = null
        executorService = Executors.newFixedThreadPool(wgSettingsConfig.regionPingThreads)
        activeTasks = []
    }

    @PreDestroy
    private void destroy() {
        log.info 'Destroying vpn region service...'
        executorService.shutdownNow()
    }

    def getRegion(String id) {
        def regions = httpClientService.fetchRegions()
        regions.find { it.id == id }
    }

    def getRegions(String sortBy = 'name') {
        def regions = httpClientService.fetchRegions()
        regions.sort { it."$sortBy" }
    }

    @Scheduled(fixedDelayString='#{@wgSettingsConfig.regionPingInterval}', initialDelayString = '#{@wgSettingsConfig.regionPingInterval}')
    void pingRegions() {
        pingRegions(false)
    }

    void pingRegions(boolean force) {
        if(!force && !appInitService.initDone) {
            log.warn 'Skipping interface test, app not initialized!'
            return
        }

        log.debug 'Starting region ping task'
        def timer = new StopWatch()
        def fastest
        final def semaphore = new Object[0]
        def regions = httpClientService.fetchRegions()
        def samples = wgSettingsConfig.regionPingSamples
        def maxWait = wgSettingsConfig.maxRegionResponseMillis
        timer.start()
        regions.each {
            it.ping = 10000
            activeTasks << executorService.submit(new Runnable() {
                @Override
                void run() {
                    StopWatch stopWatch = new StopWatch()
                    def inet = InetAddress.getByName(it.servers.wg[0].ip)
                    for (int i = 0; i < samples; ++i) {
                        stopWatch.start(i.toString())
                        def reachable = inet.isReachable(maxWait)
                        stopWatch.stop()
                        def ping = stopWatch.lastTaskTimeMillis
                        if (reachable && ping < it.ping)
                            it.ping = ping
                        if(i < samples - 1)
                            sleep 100
                    }
                    if (it.ping) {
                        synchronized (semaphore) {
                            if (!fastest || it.ping < fastest.ping)
                                fastest = it
                        }
                    }
                }
            })
        }
        activeTasks.each {it.get() }
        activeTasks.clear()
        fastestRegion = fastest
        timer.stop()
        log.debug "Region ping task completed in ${Duration.ofNanos(timer.lastTaskTimeNanos)}"
        log.debug "Fastest region is $fastestRegion.name [${fastestRegion.ping}ms]"
    }

    def getFastestRegion() {
        fastestRegion ? new JsonSlurper().parseText(JsonOutput.toJson(fastestRegion)) : null
    }

    void clearCache() {
        httpClientService.clearCache()
    }
}
