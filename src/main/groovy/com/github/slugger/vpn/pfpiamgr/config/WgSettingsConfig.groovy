/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.validation.annotation.Validated

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty

@Configuration
@ConfigurationProperties(prefix='pfpiamgr')
@Validated
class WgSettingsConfig {
    @NotEmpty String pfsenseHost
    @NotEmpty String pfsenseLogin = 'root'
    String sshKey
    @NotEmpty String portForwardRefresh = 'PT3M'
    @NotEmpty String interfaceTestInterval = 'PT2M'
    @NotEmpty String regionPingInterval = 'PT30M'
    @Min(1L) @Max(10L) int internetAliveRetries = 3
    @Min(1L) @Max(10L) int interfaceAliveRetries = 3
    @Min(50L) @Max(2000L) int maxRegionResponseMillis = 250
    @Min(3L) @Max(10L) int regionPingSamples = 3
    @Min(2L) @Max(16L) int regionPingThreads = 8
    @Min(60L) @Max(3600L) int regionCacheAgeSeconds = 1800
    @NotEmpty String piaRegionsUrl = 'https://serverlist.piaservers.net/vpninfo/servers/v4'
}