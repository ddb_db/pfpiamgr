/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.PiaConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import com.github.slugger.vpn.pfpiamgr.exceptions.WgFailure
import com.github.slugger.vpn.pfpiamgr.factories.RestClientThreadLocalFactory
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import org.apache.http.client.config.RequestConfig
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicHeader
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import java.security.KeyStore

@Service
@Slf4j
@Profile('!dev')
class HttpClientServiceImpl implements HttpClientService {

    private final PiaConfig piaConfig
    private final WgSettingsConfig wgSettingsConfig
    private final String serverListUrl

    private final ThreadLocal<RESTClient> piaRestClient
    private final ThreadLocal<RESTClient> standardRestClient
    private final Map metaServerTokens = Collections.synchronizedMap([:])
    private final Map regionsCache = Collections.synchronizedMap([:])

    @Autowired
    HttpClientServiceImpl(PiaConfig piaConfig, WgSettingsConfig wgSettingsConfig, RestClientThreadLocalFactory restClientThreadLocalFactory) {
        this.piaConfig = piaConfig
        this.wgSettingsConfig = wgSettingsConfig

        metaServerTokens = [:]
        regionsCache = [:]
        // TODO if we really aren't multithreading anymore then these two things don't need to be thread locals
        piaRestClient = restClientThreadLocalFactory.create()
        standardRestClient = restClientThreadLocalFactory.create()
    }

    void bindPort(WgInterface wgInterface, String payload, String signature) {
        def bindUrl = "https://${wgInterface.wgServer.virtualIp}:19999/bindPort"
        def http = configPiaRestClient(wgInterface.wgServer.cn)
        def resp = http.get([uri: bindUrl, query: [payload: payload, signature: signature]])
        def json = new JsonSlurper().parse(resp.data)
        log.debug "bindPort: $json"
        if(json.status != 'OK')
            throw new WgFailure('bindPort() response invalid')
    }

    def fetchPortForwardSignature(WgInterface iface) {
        def region = fetchRegions().find { it.id == iface.region }
        def token = fetchToken(region)
        def pfUrl = "https://${iface.wgServer.virtualIp}:19999/getSignature"
        def http = configPiaRestClient(iface.wgServer.cn)
        def resp = http.get([uri: pfUrl, query: [token: token]])
        new JsonSlurper().parse(resp.data)
    }

    def registerWgPublicKey(String publicKey, def region) {
        def addKeyUrl = "https://${region.servers.wg[0].ip}:1337/addKey"
        def http = configPiaRestClient(region.servers.wg[0].cn)
        http.get([uri: addKeyUrl, query: [pt: fetchToken(region), pubkey: publicKey]]).data
    }

    def fetchRegions() {
        if(!regionsCache.expires || regionsCache.expires.before(new Date())) {
            log.debug 'Region cache: MISS'
            def http = standardRestClient.get()
            def resp = http.get(uri: wgSettingsConfig.piaRegionsUrl).data
            if(regionsCache.data) {
                resp.regions.each { r ->
                    def old = regionsCache.data.regions.find { it.id == r.id }
                    if(old)
                        r.ping = old.ping
                }
            }
            def loggedData = resp
            if(!log.traceEnabled)
                loggedData = resp.toString().substring(0, Math.min(40, resp.toString().length())) + '...'
            log.debug "Fetched $serverListUrl\n$loggedData"
            regionsCache.clear()
            regionsCache['data'] = resp
            regionsCache['expires'] = new Date(System.currentTimeMillis() + 1000L * wgSettingsConfig.regionCacheAgeSeconds)
        } else
            log.debug 'Region cache: HIT'
        regionsCache['data'].regions
    }

    String fetchToken(def region) {
        def cacheKey = region.servers.meta[0].ip
        def token = metaServerTokens[cacheKey]
        if(!token || token.expires.before(new Date())) {
            log.debug "Meta server token cache: MISS [$cacheKey]"
            def http = configPiaRestClient(region.servers.meta[0].cn, piaConfig.id, piaConfig.password)
            def url = "https://$cacheKey/authv3/generateToken"
            try {
                token = new JsonSlurper().parse(http.get([uri: url]).data)
                log.debug "Fetched URL: $url\n$token"
                if(token.status == 'OK') {
                    token = [value: token.token, expires: new Date(System.currentTimeMillis() + 86400000L - 900000L)]
                    metaServerTokens[cacheKey] = token
                } else {
                    throw new IllegalStateException("Invalid status: $token.status")
                }
            } catch(Throwable t) {
                log.error "Failed to fetch token at $url", t
                metaServerTokens.remove(cacheKey)
                token = null
            }
        } else
            log.debug "Meta server token cache: HIT [$cacheKey]"
        token?.value
    }

    private RESTClient configPiaRestClient(String expectedCn, String piaId = null, String piaPwd = null, int timeoutMillis = 5000L) {
        def httpClientBuilder = HttpClients.custom()
                .setDefaultRequestConfig(buildRequestConfig(timeoutMillis))
                .setSSLSocketFactory(buildSslSocketFactory(expectedCn))
        if(piaId || piaPwd)
            httpClientBuilder.setDefaultHeaders([new BasicHeader('Authorization', 'Basic ' + "$piaId:$piaPwd".bytes.encodeBase64().toString())])

        def http = piaRestClient.get()
        http.setClient(httpClientBuilder.build())
        http
    }

    private SSLConnectionSocketFactory buildSslSocketFactory(String expectedHostname) {
        def keyStore = KeyStore.getInstance('JKS')
        keyStore.load(HttpClientServiceImpl.getResourceAsStream('/certs/pia.jks'), 'changeit'.toCharArray())
        def sslContext = new SSLContextBuilder().loadTrustMaterial(keyStore, null).build()
        new SSLConnectionSocketFactory(sslContext, new HostnameVerifier() {
            @Override
            boolean verify(String s, SSLSession sslSession) {
                sslSession.peerCertificateChain[0].getSubjectDN().name.contains("CN=${expectedHostname},")
            }
        })
    }

    private RequestConfig buildRequestConfig(int timeoutMillis) {
        RequestConfig.custom()
                .setConnectionRequestTimeout(timeoutMillis)
                .setConnectTimeout(timeoutMillis)
                .setSocketTimeout(timeoutMillis)
                .build()
    }

    @Override
    void clearCache() {
        metaServerTokens.clear()
        regionsCache.clear()
    }
}
