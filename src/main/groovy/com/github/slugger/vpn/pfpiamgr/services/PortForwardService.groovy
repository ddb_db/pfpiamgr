/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.text.SimpleDateFormat

@Slf4j
@Service
class PortForwardService {

    private final HttpClientService httpClientService
    private final WgInterfaceConfig wgInterfaceConfig
    private final VpnRegionService vpnRegionService
    private final WgCliService wgCliService

    private Date expires
    private String rawPayload
    private String signature
    private int port
    private boolean hasPort

    @Autowired
    PortForwardService(HttpClientService httpClientService, WgInterfaceConfig wgInterfaceConfig,
                        VpnRegionService vpnRegionService, WgCliService wgCliService) {
        this.httpClientService = httpClientService
        this.wgInterfaceConfig = wgInterfaceConfig
        this.vpnRegionService = vpnRegionService
        this.wgCliService = wgCliService
    }

    Date getExpires() {
        return expires
    }

    String getRawPayload() {
        return rawPayload
    }

    String getSignature() {
        return signature
    }

    int getPort() {
        return port
    }

    boolean getHasPort() {
        if(!hasPort || expires.before(new Date(System.currentTimeMillis() - 3600000L))) { // renew slightly before it expires
            def wgInterface = wgInterfaceConfig.interfaces.find {
                it.portForward && vpnRegionService.getRegion(it.region).port_forward
            }
            if(wgInterface) {
                def tries = 2 // x 3 per try
                while(tries-- > 0 && !wgCliService.isInterfaceAlive(wgInterface)) {
                    log.info "Waiting for ${wgInterface.wgServer.virtualIp} to respond"
                    sleep 500
                }
                if(tries) {
                    def payload = httpClientService.fetchPortForwardSignature(wgInterface)
                    if (payload.status == 'OK') {
                        rawPayload = payload.payload
                        signature = payload.signature
                        def response = new JsonSlurper().parse(rawPayload.decodeBase64())
                        def expiresInput = response['expires_at']
                        port = response.port
                        def fmt = new SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm:ss')
                        fmt.setTimeZone(TimeZone.getTimeZone('UTC'))
                        expires = fmt.parse(response['expires_at'].substring(0, expiresInput.lastIndexOf('.')))
                        hasPort = true
                        wgCliService.publishPortForward(port)
                    } else
                        log.error 'Did not receive expected payload from PIA'
                }
            } else
                log.debug 'No interfaces have port forwarding enabled, skipping port registration'
        }
        return hasPort
    }
}
