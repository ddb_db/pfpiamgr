/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.WgServerBuilder
import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
@Slf4j
class WgService {

    private final HttpClientService httpClientService
    private final VpnRegionService vpnRegionService
    private final WgCliService wgCliService
    private final WgInterfaceConfig wgInterfaceConfig
    private final WgSettingsConfig wgSettingsConfig
    private final WgServerBuilder wgServerBuilder
    private final PortForwardService portForwardService
    private final AppInitService appInitService

    // system state for tracking alerts
    private final Set badInterfaces
    private boolean portForwardNotified

    @Autowired
    WgService(HttpClientService httpClientService, VpnRegionService vpnRegionService,
              WgCliService wgCliService, WgInterfaceConfig wgInterfaceConfig,
              WgSettingsConfig wgSettingsConfig, WgServerBuilder wgServerBuilder,
              PortForwardService portForwardService, AppInitService appInitService) {
        this.httpClientService = httpClientService
        this.vpnRegionService = vpnRegionService
        this.wgCliService = wgCliService
        this.wgInterfaceConfig = wgInterfaceConfig
        this.wgSettingsConfig = wgSettingsConfig
        this.wgServerBuilder = wgServerBuilder
        this.portForwardService = portForwardService
        this.appInitService = appInitService

        badInterfaces = new HashSet<String>()
        portForwardNotified = false
    }

    void init() {
        createInterfacesFromConfig()
        try {
            initPortForwarding()
        } catch(Throwable t) {
            log.error 'Port forwarding init failed', t
        }
    }


    @Scheduled(fixedDelayString = '#{@wgSettingsConfig.interfaceTestInterval}')
    void testActiveInterfaces() {
        if(!appInitService.initDone) {
            log.warn 'Skipping interface test, app not initialized!'
            return
        }

        def log = log
        def wgCommandLine = wgCliService
        def recreate = false
        wgInterfaceConfig.interfaces.findAll { it.wgServer != null }.each {
            if(!wgCommandLine.isInterfaceAlive(it)) {
                if(wgCommandLine.isInternetAlive()) {
                    log.error "$it.name is not responding; recreating..."
                    it.wgServer = null
                    recreate = true
                    badInterfaces << it.name
                } else
                    log.warn "Your Internet access appears to be down, ignoring vpn interface problems."
            } else {
                log.info "$it.name: alive"
                badInterfaces.remove(it.name)
            }
        }
        if(recreate) {
            wgCliService.alert("pfpiamgr: The following interfaces stopped responding and were recreated: ${badInterfaces.join(' ')}".toString())
            wgCliService.configInterfaces()
        }
    }

    void createInterfacesFromConfig() {
        wgCliService.configInterfaces()
    }

    @Scheduled(fixedDelayString = '#{@wgSettingsConfig.portForwardRefresh}', initialDelayString = '#{@wgSettingsConfig.portForwardRefresh}')
    void initPortForwarding() {
        if(!appInitService.initDone) {
            log.warn 'Skipping port forward config, app not initialized!'
            return
        }

        def pfIfaces = wgInterfaceConfig.interfaces.findAll { it.portForward }
        if(pfIfaces.size() > 0) {
            pfIfaces.each {
                if(vpnRegionService.getRegion(it.region).port_forward) {
                    if(portForwardService.hasPort) {
                        httpClientService.bindPort(it, portForwardService.rawPayload, portForwardService.signature)
                        log.info "Port $portForwardService.port binded for $it.name successfully"
                        portForwardNotified = false
                    } else {
                        log.error 'Unable to grab a port from PIA, skipping for now'
                        if(!portForwardNotified) {
                            wgCliService.alert('pfpiamgr: Unable to acquire port from PIA for port forwarding')
                            portForwardNotified = true
                        }
                    }
                } else
                    log.warn "Skipping $it.name: $it.region does not support port forwarding"
            }
        } else
            log.info 'No interfaces have port forwarding enabled; skipping port forwarding init'
    }
}
