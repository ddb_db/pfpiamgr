/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.entities

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@ToString
@EqualsAndHashCode(includes = 'name')
class WgInterface {
    @Size(min = 1, max = 15) @Pattern(regexp='[a-zA-Z0-9]+') String name
    @Size(min = 1, max = 32) String region
    boolean portForward
    @Min(0L) @Max(65535L) int persistentKeepAlive = 25

    String ipAddress
    WgKeyPair wgKeyPair
    WgServer wgServer
}
