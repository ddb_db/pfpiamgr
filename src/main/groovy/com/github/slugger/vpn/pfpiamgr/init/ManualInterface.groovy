/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.init


import com.github.slugger.vpn.pfpiamgr.exceptions.WgFailure
import com.github.slugger.vpn.pfpiamgr.services.HttpClientService
import com.github.slugger.vpn.pfpiamgr.services.WgCliService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(4)
@Slf4j
class ManualInterface implements ApplicationRunner {

    private final HttpClientService http
    private final WgCliService wgCliService
    private ApplicationArguments arguments
    private def region

    @Autowired
    ManualInterface(HttpClientService http, WgCliService wgCliService) {
        this.http = http
        this.wgCliService = wgCliService
    }

    @Override
    void run(ApplicationArguments args) throws Exception {
        if(args.optionNames.contains('manual')) {
            arguments = args
            try {
                verifyCliArgs()
                def key = wgCliService.generateKeyPair()
                def details = http.registerWgPublicKey(key.publicKey, region)
                log.info """Here are you interface details:
[Interface]
PrivateKey = $key.privateKey
###Your PublicKey = $key.publicKey

[Peer]
PublicKey = $details.server_key
EndPoint = ${details.server_ip}:$details.server_port
AllowedIPs = ${details.peer_ip}/32,0.0.0.0/0
PersistentKeepalive = 25
"""
            } catch(WgFailure e) {
                log.error e.message
                System.exit(1)
            }
            System.exit(0)
        }
    }

    private void verifyCliArgs() {
        region = arguments.getOptionValues('region')
        if(!region)
            throw new WgFailure('Must specify --region=<region_id>')
    }
}
