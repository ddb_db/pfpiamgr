/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import com.github.slugger.vpn.pfpiamgr.entities.WgKeyPair
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.whispersystems.curve25519.Curve25519

@Slf4j
@Service
@Profile('dev')
class WgCliServiceMock implements WgCliService {

    private final WgInterfaceConfig wgInterfaceConfig
    private final WgSettingsConfig wgSettingsConfig
    private final Curve25519 curve25519

    @Autowired
    WgCliServiceMock(WgInterfaceConfig wgInterfaceConfig, WgSettingsConfig wgSettingsConfig) {
        this.wgInterfaceConfig = wgInterfaceConfig
        this.wgSettingsConfig = wgSettingsConfig
        curve25519 = Curve25519.getInstance(Curve25519.BEST)
    }

    @Override
    Set<String> fetchInterfaces() {
        wgInterfaceConfig.interfaces.collect { it.name }
    }

    @Override
    void configInterfaces() {}

    @Override
    WgKeyPair generateKeyPair() {
        def keys = curve25519.generateKeyPair()
        new WgKeyPair(publicKey: keys.publicKey.encodeBase64().toString(), privateKey: keys.privateKey.encodeBase64().toString())
    }

    @Override
    boolean isInterfaceAlive(WgInterface wgInterface) {
        true
    }

    @Override
    boolean isInternetAlive() {
        true
    }

    @Override
    void publishPortForward(int port) {

    }

    @Override
    void alert(String message) {

    }
}
