/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.init


import com.github.slugger.vpn.pfpiamgr.services.VpnRegionService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

@Component
@Order(1)
@Slf4j
class PrintHelp implements ApplicationRunner {

    @Override
    void run(ApplicationArguments args) throws Exception {
        if(args.optionNames.contains('help')) {
            log.warn '''
pfpiamgr [--help|--version|--regions]
\t--help    Print this text and exit
\t--version Print the app version and exit
\t--regions Print available PIA regions and exit

With no arguments, start the background service with the default properties file.
'''
            System.exit(1)
        }
    }
}
