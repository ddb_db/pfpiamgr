/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.builders.WgServerBuilder
import com.github.slugger.vpn.pfpiamgr.config.WgInterfaceConfig
import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import com.github.slugger.vpn.pfpiamgr.entities.WgKeyPair
import com.github.slugger.vpn.pfpiamgr.exceptions.WgFailure
import groovy.io.FileType
import groovy.json.JsonOutput
import groovy.util.logging.Slf4j
import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.connection.channel.direct.Session
import net.schmizz.sshj.transport.verification.PromiscuousVerifier
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.whispersystems.curve25519.Curve25519

import java.text.SimpleDateFormat

@Slf4j
@Service
@Profile('!dev')
class WgCliServiceImpl implements WgCliService {

    private final HttpClientService http
    private final WgServerBuilder wgServerBuilder
    private final VpnRegionService vpnRegionService
    private final WgInterfaceConfig wgInterfaceConfig
    private final WgSettingsConfig wgSettingsConfig
    private final SshService sshService
    private final SystemService systemService

    private final Curve25519 curve25519
    private final Map<String, Integer> assignedInterfaces

    private def xmlSlurper
    private int lastPort

    @Autowired
    WgCliServiceImpl(HttpClientService http, WgServerBuilder wgServerBuilder,
                     VpnRegionService vpnRegionService, WgInterfaceConfig wgInterfaceConfig,
                    WgSettingsConfig wgSettingsConfig, SshService sshService, SystemService systemService) {
        this.http = http
        this.wgServerBuilder = wgServerBuilder
        this.vpnRegionService = vpnRegionService
        this.wgInterfaceConfig = wgInterfaceConfig
        this.wgSettingsConfig = wgSettingsConfig
        this.sshService = sshService
        this.systemService = systemService
        curve25519 = Curve25519.getInstance(Curve25519.BEST)
        assignedInterfaces = Collections.synchronizedMap(new HashMap<String, Integer>())
    }

    private void refreshConfig(boolean initOnly = false) {
        if(initOnly && xmlSlurper)
            return
        def result = sshService.exec('cat /cf/conf/config.xml', true)
        if(result.status)
            throw new WgFailure('Error fetching pfSense config')
        xmlSlurper = new XmlSlurper().parseText(result.stdout)
    }

    @Override
    Set<String> fetchInterfaces() {
        refreshConfig(true)
        xmlSlurper.wireguard.tunnel.'*'.findAll { it.name.text() =~ /pia.+/ }.collect { it.name.text() }
    }

    private Map buildInterface(WgInterface iface) {
        def keyPair = generateKeyPair()
        def region = vpnRegionService.getRegion(iface.region)
        def details = http.registerWgPublicKey(keyPair.publicKey, region)
        def ifaceMap = [
            name: "wg${findInterfaceNumber(iface.name)}",
            enabled: 'yes',
            descr: "[pfpiamgr/$iface.name]: $region.name",
            interface: [
                address: "${details.peer_ip}/32",
                listenport: 0,
                privatekey: keyPair.privateKey,
                publickey: keyPair.publicKey
            ],
            peers: [
                wgpeer: [[
                     publickey: details.server_key,
                     endpoint: details.server_ip,
                     port: details.server_port,
                     allowedips: "${details.server_vip}/32,0.0.0.0/0",
                     descr: null,
                     persistentkeepalive: 25,
                     presharedkey: null,
                     peerwgaddr: details.server_vip
                ]]
            ]
        ]
        iface.wgServer = wgServerBuilder
            .withIp(details.server_ip)
            .withPort(details.server_port)
            .withVirtualIp(details.server_vip)
            .withPublicKey(details.server_key)
            .withDnsServers(details.dns_servers)
            .withCn(region.servers.wg[0].cn)
            .build()
        iface.ipAddress = details.peer_ip
        iface.wgKeyPair = keyPair
        ifaceMap
    }

    private Collection<Map> buildNewInterfaces() {
        def newIfaces = wgInterfaceConfig.interfaces.findAll { it.wgServer == null }
        def ifaces = []
        newIfaces.each {
            try {
                ifaces << buildInterface(it)
            } catch(Throwable t) {
                it.wgServer = null
                log.error "Skipped $it.name", t
            }
        }
        ifaces
    }

    private Collection<Map> buildExistingInterfaces() {
        def existing = wgInterfaceConfig.interfaces.findAll { it.wgServer != null }
        existing.collect {
            def wgServer = it.wgServer
            def region = vpnRegionService.getRegion(it.region)
            [
                name: "wg${findInterfaceNumber(it.name)}",
                enabled: 'yes',
                descr: "[pfpiamgr/$it.name]: $region.name",
                interface: [
                    address: "${it.ipAddress}/32",
                    listenport: 0,
                    privatekey: it.wgKeyPair.privateKey,
                    publickey: it.wgKeyPair.publicKey
                ],
                peers: [
                    wgpeer: [[
                         publickey: wgServer.publicKey,
                         endpoint: wgServer.ip,
                         port: wgServer.port,
                         allowedips: "${wgServer.virtualIp}/32,0.0.0.0/0",
                         descr: null,
                         persistentkeepalive: 25,
                         presharedkey: null,
                         peerwgaddr: wgServer.virtualIp
                    ]]
                ]
            ]
        }
    }

    @Override
    void configInterfaces() {
        refreshConfig(true)
        def wireguardConfig = [tunnel: []]
        wireguardConfig.tunnel.addAll(buildExistingInterfaces())
        wireguardConfig.tunnel.addAll(buildNewInterfaces())
        def updatedInterfaces = wireguardConfig.tunnel.collect { it.name }
        def assignedInterfaces = findAssignedInterfaces().asList()
        if(updatedInterfaces.intersect(assignedInterfaces) != assignedInterfaces)
            throw new WgFailure('Cannot remove PIA interfaces that are assigned in pfSense')

        def gateways = new HashSet<String>()
        def liveInterfaces = wgInterfaceConfig.interfaces.findAll { it.wgServer }
        gateways.addAll(liveInterfaces.collect {it.wgServer.virtualIp })
        if(gateways.size() != liveInterfaces.size()) {
            vpnRegionService.clearCache()
            throw new WgFailure('Interface configuration halted due to detected gateway conflict')
        }
        copyConfigToRouter(JsonOutput.toJson(wireguardConfig))
        applyConfigToRouter()
        refreshConfig()
    }

    private void applyConfigToRouter() {
        log.info 'Applying updated WireGuard config to pfSense...'
        def result = sshService.exec('pfSsh.php playback pfpiamgr_reloadwg')
        log.info 'pfSense update completed'
        if(result.status)
            throw new WgFailure('Failed to apply updated WireGuard config')
    }

    private void copyConfigToRouter(String payload) {
        def f = File.createTempFile('__pfpiamgr_wgcfg_', '.json')
        try {
            f << payload
            sshService.copyFileTo('/tmp/__pfpiamgr_wgcfg.json', f)
        } finally {
            f.delete()
        }
    }

    @Override
    WgKeyPair generateKeyPair() {
        def keys = curve25519.generateKeyPair()
        new WgKeyPair(publicKey: keys.publicKey.encodeBase64().toString(), privateKey: keys.privateKey.encodeBase64().toString())
    }

    @Override
    boolean isInterfaceAlive(WgInterface wgInterface) {
        def retries = wgSettingsConfig.interfaceAliveRetries
        while(retries-- > 0) {
            if(!sshService.exec("ping -q -c 1 $wgInterface.wgServer.virtualIp".toString()).status)
                return true
            systemService.doSleep(1000)
        }
        false
    }

    @Override
    boolean isInternetAlive() {
        def retries = wgSettingsConfig.internetAliveRetries
        while(retries-- > 0) {
            if(!sshService.exec('ping -q -c 1 8.8.8.8').status)
                return true
            systemService.doSleep(1000)
        }
        false
    }

    @Override
    void publishPortForward(int port) {
        if(!port || lastPort == port)
            return

        def targetDir = '/usr/local/www/pfpiamgr'
        def result = sshService.exec("sh -c 'mkdir -p $targetDir; echo $port > $targetDir/port.txt'")
        if(result.status)
            log.error 'Failed to publish port forward to router'
        else
            lastPort = port
    }

    @Override
    void alert(String message) {
        def fmt = new SimpleDateFormat('yyyyMMddHHmmss')
        fmt.setTimeZone(TimeZone.getTimeZone('UTC'))
        def ts = fmt.format(new Date())
        sshService.exec("pfSsh.php playback pfpiamgr_notices_create pfpiamgr-$ts \"$message\"")
    }

    private Set<String> findAssignedInterfaces() {
        refreshConfig(true)
        def allAssigned = xmlSlurper.interfaces.'*'
                .findAll { it.if.text() =~ /wg.+/ }
                .collect { it.if.text() }
        def piaInterfaces = xmlSlurper.wireguard.tunnel
                .findAll { it.descr.text().startsWith('[pfpiamgr/') }
                .collect { it.name.text() }
        piaInterfaces.intersect(allAssigned)
    }

    private int findInterfaceNumber(String name) {
        refreshConfig(true)
        def iface = xmlSlurper.wireguard.tunnel.find { it.descr.text().startsWith("[pfpiamgr/$name]:") }
        if(iface)
            iface.name.text().substring(2).toInteger()
        else if(assignedInterfaces[name])
            assignedInterfaces[name]
        else {
            def ids = xmlSlurper.wireguard.tunnel.collect { it.name.text().substring(2).toInteger() }
            ids.addAll(assignedInterfaces.values())
            int id = -1
            while(++id >= 0)
                if(!ids.contains(id)) {
                    assignedInterfaces[name] = id
                    return id
                }
        }
    }
}
