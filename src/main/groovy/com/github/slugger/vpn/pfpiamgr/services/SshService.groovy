/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.WgSettingsConfig
import com.github.slugger.vpn.pfpiamgr.exceptions.WgFailure
import groovy.io.FileType
import groovy.util.logging.Slf4j
import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.connection.channel.direct.Session
import net.schmizz.sshj.transport.verification.PromiscuousVerifier
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
class SshService {

    static class ExecResult {
        int status
        String stdout
        String stderr
    }

    private final WgSettingsConfig wgSettingsConfig
    private SSHClient ssh

    @Autowired
    SshService(WgSettingsConfig wgSettingsConfig) {
        this.wgSettingsConfig = wgSettingsConfig
    }

    void copyFileTo(String dest, File src) {
        if(!connected)
            connect()
        ssh.newSCPFileTransfer().upload(src.absolutePath, dest)
    }

    void copyFileFrom(String src, File dest) {
        if(!connected)
            connect()
        ssh.newSCPFileTransfer().download(src, dest.absolutePath)
    }

    ExecResult exec(String command, boolean sensitive = false) {
        if(!connected)
            connect()
        log.debug "Remote execution: $command"
        ExecResult result = new ExecResult()
        Session session = ssh.startSession()
        session.withCloseable {
            Session.Command cmd = session.exec(command)
            cmd.withCloseable {
                cmd.join()
            }
            result.status = cmd.exitStatus
            result.stdout = cmd.inputStream.text
            result.stderr = cmd.errorStream.text
        }
        if(result.status || log.isDebugEnabled()) {
            def level = result.status ? 'error' : 'debug'
            log."$level" "RC: $result.status"
            if(result.stdout) {
                if(sensitive) {
                    log."$level" 'stdout: Sensitive output suppressed; enable trace logging to see this output'
                    log.trace "stdout:\n$result.stdout"
                } else
                    log."$level" "stdout:\n$result.stdout"
            }
            if(result.stderr) {
                if(sensitive) {
                    log."$level" 'stderr: Sensitive output suppressed; enable trace logging to see this output'
                    log.trace "stderr:\n$result.stderr"
                } else
                    log."$level" "stderr:\n$result.stderr"
            }
        }
        result
    }

    private boolean isConnected() {
        ssh?.isConnected()
    }

    private void syncRouterFiles() {
        def src = new File(new File(System.getenv('APP_HOME') ?: './src/dist'),'pfsense')
        src.eachFileRecurse(FileType.FILES) {
            def target = "/${src.relativePath(it).toString()}"
            copyFileTo(target, it.absoluteFile)
        }
    }

    private void connect() {
        ssh = new SSHClient()
        ssh.addHostKeyVerifier(new PromiscuousVerifier())
        ssh.connect(wgSettingsConfig.pfsenseHost)
        if(wgSettingsConfig.sshKey)
            ssh.authPublickey(wgSettingsConfig.pfsenseLogin, wgSettingsConfig.sshKey)
        else
            ssh.authPublickey(wgSettingsConfig.pfsenseLogin)
        verifyPfSenseVersion()
        syncRouterFiles()
    }

    private void verifyPfSenseVersion() {
        def result = exec('cat /etc/version')
        if(result.status)
            throw new WgFailure('Unable to determine pfSense version')
        def version = result.stdout.split('\\.')
        if(version[0].toInteger() < 2 || version[1].toInteger() < 5)
            throw new WgFailure('pfpiamgr requires pfSense version 2.5.0-RELEASE or newer')
        else
            log.debug "Identified pfSense version $result.stdout"
    }
}
