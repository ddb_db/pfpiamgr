/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import com.github.slugger.vpn.pfpiamgr.config.PiaConfig
import com.github.slugger.vpn.pfpiamgr.entities.WgInterface
import com.github.slugger.vpn.pfpiamgr.factories.RestClientThreadLocalFactory
import groovy.json.JsonOutput
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.whispersystems.curve25519.Curve25519

import java.text.SimpleDateFormat

@Service
@Slf4j
@Profile('dev')
class HttpClientServiceMock implements HttpClientService {

    private final String serverListUrl

    private final ThreadLocal<RESTClient> standardRestClient
    private final Map metaServerTokens = Collections.synchronizedMap([:])
    private final Map regionsCache = Collections.synchronizedMap([:])
    private final Random rng = new Random()

    @Autowired
    HttpClientServiceMock(PiaConfig piaConfig,
                          @Value('${wgmgr.serverlist.url:https://serverlist.piaservers.net/vpninfo/servers/v4}') String serverListUrl,
                          RestClientThreadLocalFactory restClientThreadLocalFactory) {
        this.serverListUrl = serverListUrl

        metaServerTokens = Collections.synchronizedMap([:])
        regionsCache = Collections.synchronizedMap([:])
        standardRestClient = restClientThreadLocalFactory.create()
    }

    void bindPort(WgInterface wgInterface, String payload, String signature) {}

    def fetchPortForwardSignature(WgInterface iface) {
        def fmt = new SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm:ss.SSS')
        fmt.setTimeZone(TimeZone.getTimeZone('UTC'))
        def expiresAt = fmt.format(new Date(System.currentTimeMillis() + 90L * 86400000L))
        [
                status: 'OK',
                payload: JsonOutput.toJson([expires_at: expiresAt, port: rng.nextInt(20000) + 32000]).bytes.encodeBase64().toString(),
                signature: '123',
        ]
    }

    def registerWgPublicKey(String publicKey, def region) {
        [
                status: 'OK',
                server_ip: '192.168.254.250',
                server_port: 1337,
                server_vip: '10.0.0.1',
                server_key: Curve25519.getInstance(Curve25519.BEST).generateKeyPair().publicKey.encodeBase64().toString(),
                dns_servers: ['8.8.8.8'],
                peer_ip: '10.0.0.254'
        ]
    }

    def fetchRegions() {
        if(!regionsCache.expires || regionsCache.expires.before(new Date())) {
            log.debug 'Region cache: MISS'
            def http = standardRestClient.get()
            def resp = http.get(uri: serverListUrl).data
            if(regionsCache.data) {
                resp.regions.each { r ->
                    def old = regionsCache.data.regions.find { it.id == r.id }
                    if(old)
                        r.ping = old.ping
                }
            }
            def loggedData = resp
            if(!log.traceEnabled)
                loggedData = resp.toString().substring(0, Math.min(40, resp.toString().length())) + '...'
            log.debug "Fetched $serverListUrl\n$loggedData"
            regionsCache.clear()
            regionsCache['data'] = resp
            // TODO make the cache timeout configurable
            regionsCache['expires'] = new Date(System.currentTimeMillis() + 1800000L)
        } else
            log.debug 'Region cache: HIT'
        regionsCache['data'].regions
    }

    String fetchToken(def region) {
        def cacheKey = region.servers.meta[0].ip
        def token = metaServerTokens[cacheKey]
        if(!token || token.expires.before(new Date())) {
            log.debug "Meta server token cache: MISS [$cacheKey]"
            token = [value: 'PiaAuthToken']
        } else
            log.debug "Meta server token cache: HIT [$cacheKey]"
        token?.value
    }

    @Override
    void clearCache() {}
}
