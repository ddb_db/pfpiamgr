/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AppVersionService {

    private final Properties buildProperties

    @Autowired
    AppVersionService() {
        buildProperties = new Properties()
        init(AppVersionService.class.getResourceAsStream('/version.properties'))
    }

    AppVersionService(File input) {
        buildProperties = new Properties()
        init(input.newInputStream())
    }

    AppVersionService(String input) {
        buildProperties = new Properties()
        new StringReader(input).withReader {
            buildProperties.load(it)
        }
    }

    String getProperty(String name) {
        buildProperties["VERSION_${name.toUpperCase()}"]
    }

    private void init(InputStream input) {
        def buildProperties = buildProperties
        input.withStream {
            buildProperties.load(it)
        }
    }
}
