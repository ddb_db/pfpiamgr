/*
    pfpiamgr: A PIA WireGuard interface manager for pfSense

    Copyright (C) 2021 Battams, Derek <derek@battams.ca>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package com.github.slugger.vpn.pfpiamgr.builders

import com.github.slugger.vpn.pfpiamgr.services.CommandLineService
import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import org.springframework.stereotype.Component

@Component
@Builder(forClass = CommandLineService.ExecResult, builderStrategy = ExternalStrategy, prefix = 'with')
class ExecResultBuilder {}
